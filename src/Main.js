import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import { Nurse, Admin, Provider } from './pages'
class Main extends Component {
    render() {
        return (
            <div>
                <Switch>
                    {/* <Route path="/login" component={login} /> */}
                    <Route path="/admin" component={Admin} />
                    <Route path="/provider" component={Provider} />
                    <Route path="/nurse" component={Nurse} />
                    {/* <Route component={NoMatch} /> */}
                </Switch>
            </div>
        )
    }
}
export default Main
