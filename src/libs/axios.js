import axios, {create} from 'axios'

const API_END_POINT = 'http://localhost:3000/api/v1/';

export function createApi(options) {
	return create(options)
}

export function setHeader(key, value, api) {
	(api || axios).defaults.headers.common[key] = value;
}

export const adminApi = create({
	baseURL: API_END_POINT,
	headers: {
		'Accept': 'application/json',
		'X-Requested-With': 'XMLHttpRequest'
	}
})

export const providerApi = create({
	baseURL: API_END_POINT,
	headers: {
		'Accept': 'application/json',
		'X-Requested-With': 'XMLHttpRequest'
	}
})

export const nurseApi = create({
	baseURL: API_END_POINT,
	headers: {
		'Accept': 'application/json',
		'X-Requested-With': 'XMLHttpRequest'
	}
})