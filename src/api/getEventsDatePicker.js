import { nurseApi } from '../libs/axios'

export default getEventsDatePicker =( deparments,providers )=> {
    nurseApi.post('getEventsDatePicker', {
        data: {
            deparments,providers
        }
    })
}