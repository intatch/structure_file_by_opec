import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import  Typography  from "@material-ui/core/Typography";
const styles = theme => ({
    period: {
        "&:hover": {
            backgroundColor: "rgba(0, 0, 0, 0.4)",
        },
        "&:nth-of-type(even)": {
            backgroundColor: "rgba(0, 0, 0, 0.03)",
            "&:hover": {
                backgroundColor: "rgba(0, 0, 0, 0.4)",
            }
        },
        display: 'flex'
    },
    appointment: {
        display: "flex",
        position: "relative",
        backgroundColor: "rgba(255, 0, 0, 0.2)",
        width: "100%",
        "&:nth-of-type(even)": {
            backgroundColor: "rgba(205, 0, 0, 0.3)",
            "&:hover": {
                backgroundColor: "rgba(255, 0, 0, 0.4)",
            }
        },
        "&:hover": {
            backgroundColor: "rgba(255, 0, 0, 0.4)"
        },
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    triangle: {
        borderStyle: "solid",
        borderWidth: "0 30px 30px 0",
        borderColor: "transparent rgba(32, 42, 38, 0.9) transparent transparent",
        position: "absolute",
        zIndex: "10",
        right: 0,
        zIndex: 99999,
        "&:hover": {
            borderColor: "transparent rgba(124, 190, 164, 0.9) transparent transparent"
        }
    }
});

const setTimeInTime = (timeScope, heightOfRow) => {
    console.log(parseInt(timeScope) * (heightOfRow / 60))
    return {
        width: "100%",
        height: `${parseInt(timeScope) * (parseInt(heightOfRow) / 60)}px`
    };
};
function Period(props) {
    let {
        classes,
        appointment,
        heightOfRow,
        handleModel
    } = props;
    let { minute, timeScope, customerName, insert, type } = appointment
    if (type === "empty") {
        return (
            <div
                className={classes.period}
                style={setTimeInTime(appointment.timeScope, heightOfRow)}
                onClick={() => { handleModel() }}
            >
                <Typography style={{ marginTop: 'auto', marginBottom: 'auto' }} variant="h6">{appointment.minute}</Typography>
            </div>
        );
    } else {
        return (
            <div className={classes.appointment} style={setTimeInTime(appointment.timeScope, heightOfRow)}>
                {insert.length !== 0 && <div onClick={() => { console.log('test') }} className={classes.triangle}></div>}
                <div
                    onClick={() => { console.log('test2') }}
                    style={{ width: "100%", }}
                >
                    <Typography style={{ marginTop: 'auto', marginBottom: 'auto' }} variant="h6">{appointment.minute}</Typography>
                    <Typography style={{ marginTop: 'auto', marginBottom: 'auto' }} variant="h6">{appointment.name}</Typography>
                </div>
            </div>

        );
    }
}

Period.propTypes = {
    classes: PropTypes.object.isRequired,
    minute: PropTypes.string.isRequired,
    timeScope: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    customerName: PropTypes.string.isRequired,
    heightRow: PropTypes.string.isRequired
};
Period.defaultProps = {
    classes: {},
    minute: "",
    timeScope: "",
    type: "empty",
    customerName: "",
    heightRow: ""
};

export default withStyles(styles)(Period)
