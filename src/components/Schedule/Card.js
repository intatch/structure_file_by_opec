import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'
import moment from 'moment'
import Period from './Period.js'
const styles = theme => ({
    root: {
        width: '93%',
        zIndex: 5,
        borderButtomStyle: 'solid'
    },
    position2: {
        position: 'relative',
        zIndex: 1,
        width: '100%',
        margin: '0',
        padding: '0',
        border: '0',
    }
})

const setTime = (start, end, height, openingTime) => {
    const heightInt = parseInt(height)
    const startTime = moment(`2014-11-11 ${start}:00`)
    const endTime = moment(`2014-11-11 ${end}:00`)
    const openTime = moment(`2014-11-11 ${openingTime[0]}:00`)
    return {
        height: `${(endTime.diff(startTime, 'minute') / 60) * heightInt}px`,
        top: `${(startTime.diff(openTime, 'minute') / 60) * heightInt}px`
    }
}
const setTimeInTime = (timeScope, heightOfRow) => {
    return {
        height: `${parseInt(heightOfRow) / 60 * timeScope}px`
    };
};
function CardProvider(props) {
    const { classes, heightOfRow, provider, openingHours, handleModel } = props
    const { name, start, end, type, appointments } = provider
    if (type === '') {
        return (
            <div
                className={classes.position2}
                style={setTime(start, end, heightOfRow, openingHours)}
            >
                {appointments.map(appointment => {
                    return <Period appointment={appointment} handleModel={handleModel} heightOfRow={heightOfRow} />
                })}
            </div>
        )
    } else if (type === 'onLeave') {
        return (
            <div
                className={classes.position2}
                stlye={setTime(start, end, heightOfRow, openingHours)}
            >
                {appointments.map(appointment => {
                    // return <Period heightOfRow={heightOfRow} appointment={appointment} />
                })}
            </div>
        )
    }
}

CardProvider.propTypes = {
    classes: PropTypes.object.isRequired,
    heightOfHouse: PropTypes.string.isRequired,
    providerName: PropTypes.string.isRequired,
    timeStart: PropTypes.string.isRequired,
    timeEnd: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    appointments: PropTypes.array.isRequired
}

CardProvider.defaultProps = {
    heightOfHouse: '',
    providerName: '',
    timeStart: '',
    timeEnd: '',
    type: '',
    appointments: []
}
export default withStyles(styles)(CardProvider)
