import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import Slide from '@material-ui/core/Slide'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import CardContent from '@material-ui/core/CardContent'
import Card from '@material-ui/core/Card'
import CloseIcon from '@material-ui/icons/Close'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import dateFns from 'date-fns'
// import InputMulti from './InputMulti'
// import SelectSi from './SelectSi'
const styles = theme => ({
    appBar: {
        position: 'relative'
    },
    flex: {
        flex: 1
    },
    area: {},
    dd: {
        minHeight: '80vh',
        maxHeight: '80vh',
        overflow: 'initial',
        overflowY: 'initial'
    },
    backButton: {
        marginRight: theme.spacing.unit,
    },
    instructions: {
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
    },
    textField: {
        paddingTop: theme.spacing.unit,
        paddingButtom: theme.spacing.unit,
    },
})

function getSteps() {
    return ['ค้นหารายชื่อ', 'กรอกประวัติ', 'นัดหมาย', 'ยืนยันการยัดหมาย']
}
function Transition(props) {
    return <Slide style={{ overflow: 'initial', overflowY: 'initial' }} direction='up' {...props} />
}

class FullScreenDialog extends React.Component {
    state = {
        activeStep: 0,
        HN: "",
        name: "",
        phone: "",
    }
    findName = name => event => {
        this.setState({
            [name]: event.target.value,
        })
        let data = customer.filter(v => v[name] === event.target.value)
        if (data.length === 1) {
            this.setState({ name: data[0].name, HN: data[0].HN, phone: data[0].phone, expanded: true, address: data[0].address, Blood: data[0].Blood, Birthday: data[0].date, congenitalDisease: data[0].congenitalDisease, gender: data[0].gender, })
            // this.props.handleChange('name')(data[0].name)
        }
    }
    handleChange = (name) => value => {
        this.setState({ [name]: value })
    }

    render() {
        const { classes, handleClose, start, name, addArr, addAppointment, id, user, end, handleChange, sm, room, sy, dentist, handleModel, openModal } = this.props
        const { activeStep } = this.state
        const steps = getSteps()
        const getStepContent = (stepIndex) => {
            switch (stepIndex) {
                case 0:
                    return <div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                id='outlined-HN'
                                fullWidth
                                label='HN*'
                                name='HN'
                                onChange={(value) => {
                                    this.findName('HN')(value)
                                    this.handleChange('HN')(value.target.value)
                                }}
                                value={this.state.HN}
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                required
                                fullWidth
                                id='outlined-name'
                                label='ชื่อ'
                                value={this.state.name}
                                onChange={(value) => {
                                    // handleChange('name')(value.target.value)
                                    this.handleChange('name')(value.target.value)
                                    this.findName('name')(value)
                                }}
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                required
                                fullWidth
                                onChange={(value) => {
                                    this.handleChange('phone')(value.target.value)
                                    this.findName('phone')(value)
                                }}
                                id='outlined-required'
                                label='เบอร์'
                                value={this.state.phone}
                                className={classes.textField}
                            />
                        </div>

                    </div>
                case 1:
                    return <div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                required
                                fullWidth
                                id='outlined-name'
                                label='ชื่อ'
                                value={this.props.name}
                                onChange={(value) => {
                                    handleChange('name')(value.target.value)
                                    this.handleChange('name')(value.target.value)
                                }}
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                required
                                fullWidth
                                id='outlined-name'
                                label='นามสกุล'
                                value={this.state.lastName}
                                onChange={(value) => {
                                    this.handleChange('lastName')(value.target.value)
                                }}
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                required
                                fullWidth
                                id='outlined-name'
                                label='เบอร์'
                                value={this.state.phone}
                                onChange={(value) => {
                                    this.handleChange('phone')(value.target.value)
                                }}
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                fullWidth
                                id='outlined-address'
                                label='ที่อยู่'
                                value={this.state.address}
                                onChange={(e) => this.handleChange('address')(e.target.value)}
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                fullWidth
                                id='outlined-Blood'
                                label='กรุ๊ปเลือด'
                                onChange={(e) => this.handleChange('Blood')(e.target.value)}
                                value={this.state.Blood}
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                fullWidth
                                type='date'
                                id='outlined-error'
                                label='วันเกิด'
                                onChange={(e) => this.handleChange('birthday')(e)}
                                value={this.state.birthday}
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                fullWidth
                                value={this.state.gender}
                                onChange={(e) => this.handleChange('gender')(e.target.value)}
                                id='outlined-gender'
                                label='เพศ'
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            {/* <InputMulti
                                fullWidth
                                label='โรคประจำตัว'
                                handleChange={(e) => this.handleChange('congenitalDisease')(e)}
                                value={this.state.congenitalDisease}
                                suggestions={[
                                    'หลอดเลือดหัวใจ',
                                    'เบาหวาน',
                                    'ความดันโลหิตสูง',
                                    'ปอดเรื้อรัง',
                                    'ระบบกล้ามเนื้อ เส้นเอ็นอักเสบ']} /> */}
                        </div>
                    </div>
                case 2:
                    return <div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                fullWidth
                                id='outlined-error'
                                style={{ color: '#000' }}
                                type='date'
                                label='date'
                                value='2017-10-31'
                                onChange={(value) => { }}
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                fullWidth
                                onChange={(value) => { handleChange('start')(value.target.value) }}
                                type='time'
                                id='outlined-From'
                                label='From'
                                value={this.props.start}
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            <TextField
                                fullWidth
                                onChange={(value) => { handleChange('end')(value.target.value) }}
                                type='time'
                                id='outlined-To'
                                label='To'
                                value={this.props.end}
                                className={classes.textField}
                            />
                        </div>
                        <div style={{ padding: '10px' }}>
                            {/* <SelectSi label='แพทย์' handleChange={handleChange('dentist')} placeholder='Dentist' value={{ label: this.props.dentist }} className={classes.textField} suggestions={[
                                'นพ. ณัชชา',
                                'นพ. สมจิต',
                                'นพ. สมชาย',
                                'นพ. สมควร'
                            ]} /> */}
                        </div>
                        <div style={{ padding: '10px' }}>
                            {/* <InputMulti handleChange={handleChange('sy')} label='รักษา' placeholder='SM' value={this.props.sy} className={classes.textField} suggestions={[
                                'ถอนฟัน',
                                'ขัดฟัน',
                                'ดัดฟัน',
                                'ฟอกฟัน',
                                'ect.']} /> */}
                        </div>
                        <div style={{ padding: '10px' }}>
                            {/* <SelectSi
                                label='ห้อง'
                                handleChange={handleChange('room')}
                                value={this.props.room}
                                placeholder='Room'
                                className={classes.textField}
                                suggestions={[
                                    'a1',
                                    'a2',
                                    'a3',
                                    'a4',
                                    'a5']} /> */}
                        </div>
                    </div>
                case 3:
                    return <Grid style={{ padding: '40px' }} container spacing={24}  >
                        <Grid container justify='center'>
                            <Typography variant='h2' style={{ textAlign: 'center', textDecoration: 'underline', margin: '0px 40px 40px 40px' }} className={classes.grow}>
                                ข้อมูลนัดหมาย
                        </Typography>
                        </Grid>
                        <Grid container justify='center' spacing={24}>
                            <Grid item xs={3}>
                                <img onClick={this.handleDrawerOpen} src='http://blueseas.co.th/img/logo.png' style={{ marginRight: '1%' }} />
                                <Typography variant='h6' style={{ width: '5%', display: 'flex', color: '#3fa9f5' }} className={classes.grow}>
                                    BlueSeas
                                </Typography>
                                <Typography variant='body1' color='inherit' className={classes.flex}>
                                    contact@blueseas.co.th
                                    02-878-5567
                                </Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <Typography variant='h6' color='inherit' className={classes.flex}>
                                    HN: {`10001`}
                                </Typography>
                                <Typography variant='h6' color='inherit' className={classes.flex}>
                                    {`วันที่: 2017-05-24`}
                                </Typography>
                                <Typography variant='h6' color='inherit' className={classes.flex}>
                                    {`เวลา: ${this.props.start}`}
                                </Typography>
                                <Typography variant='h6' color='inherit' className={classes.flex}>
                                    {`ถึง: ${end}`}
                                </Typography>
                                <Typography variant='h6' color='inherit' className={classes.flex}>
                                    ชื่อ: {this.props.name}
                                </Typography>
                            </Grid>
                            <Grid item xs={5}>
                                <Typography variant='h6' color='inherit' className={classes.flex}>
                                    {`ห้อง: ${this.props.room.label}`}
                                </Typography>
                                <Typography variant='h6' color='inherit' className={classes.flex}>
                                    {`แพทย์: ${this.props.dentist}`}
                                </Typography>
                                <Typography variant='h6' color='inherit' className={classes.flex}>
                                    อาการ: {this.props.sy.map(v => `  ${v.label}`)}
                                </Typography>
                                <Typography variant='h6' color='inherit' className={classes.flex}>
                                    ออกหมายนัดโดย: {`admin วรปรีย์ นามสกุล`}
                                </Typography>
                                <Typography variant='h6' color='inherit' className={classes.flex}>
                                    วันที่ออกหมายนัด: {dateFns.format(new Date(), 'D / M / YYYY  hh : mm')}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid >

                //         {name: name, start: this.props.start, end: end, symptoms: sy.map(v => { return { name: v.value, amount: '1' } })}
                default:
                    return 'Uknown stepIndex'
            }
        }
        return (
            <div style={{ overflow: 'initial', overflowY: 'initial' }}>
                <Dialog
                    style={{ overflow: 'initial', overflowY: 'initial', width: '100%', maxWidth: 'none' }}
                    className={classes.dd}
                    open={openModal}
                    onClose={() => {

                    }}
                    TransitionComponent={Transition}
                    fullWidth={true}
                    maxWidth={'lg'}
                >
                    <AppBar className={classes.appBar}>
                        <Toolbar>
                            <IconButton
                                onClick={handleModel}
                                aria-label='Close'
                            >
                                <CloseIcon />
                            </IconButton>
                            <Typography variant='h6' color='inherit' className={classes.flex}>
                                นัดหมาย
                            </Typography>
                            {activeStep === 0 &&
                                <Button variant='text'
                                    style={{ background: '#fff', color: '#3f51b5', fontWeight: 'bold' }} onClick={() => {
                                        if (this.state.HN !== '10001' || this.state.HN !== '10002' || this.state.HN !== '10003') {
                                            this.setState({ activeStep: 1 })
                                        }
                                        if (this.state.HN === '10001' || this.state.HN === '10002' || this.state.HN === '10003') {
                                            this.setState({ activeStep: 2 })
                                        }
                                    }}>
                                    กรอกประวัติ
                            </Button>}
                            {activeStep === 1 &&
                                <Button variant='text'
                                    style={{ background: '#fff', color: '#3f51b5', fontWeight: 'bold' }} onClick={() => {
                                        this.setState({ activeStep: 2 })
                                    }}>
                                    เพิ่มข้อมูล
                            </Button>}
                            {activeStep === 2 &&
                                <Button variant='text'
                                    style={{ background: '#fff', color: '#3f51b5', fontWeight: 'bold' }} onClick={() => {
                                        this.setState({ activeStep: 3 })
                                    }}>
                                    นัดหมาย
                                </Button>}
                            {activeStep === 3 &&
                                <Button variant='text'
                                    style={{ background: '#fff', color: '#3f51b5', fontWeight: 'bold' }} onClick={() => {
                                        addAppointment({ name: this.state.name, start: this.props.start, end: end, symptoms: sy.map(v => { return { name: v.value, amount: '1' } }), type: 'appointment', room: room.value }, id)
                                        handleClose()
                                        this.setState({ activeStep: 0, name: '', sy: [], room: '', name: '', HN: '', phone: '', address: '', Blood: '', gender: '', congenitalDisease: [], })
                                        // addArr()
                                    }}>
                                    ยืนยันนัดหมาย
                            </Button>}
                        </Toolbar>
                    </AppBar>
                    <Card style={{ overflowY: 'initial', overflow: 'initial' }} className={classes.card} >
                        <CardContent style={{ overflowY: 'initial', overflow: 'initial' }}  >
                            <Stepper activeStep={activeStep} alternativeLabel>
                                {steps.map(label => {
                                    return (
                                        <Step key={label}>
                                            <StepLabel>{label}</StepLabel>
                                        </Step>
                                    )
                                })}
                            </Stepper>
                            {getStepContent(activeStep)}
                        </CardContent>
                    </Card >
                </Dialog>
            </div >
        )
    }
}

FullScreenDialog.propTypes = {
    classes: PropTypes.object.isRequired
}

export default withStyles(styles)(FullScreenDialog)
const customer = [
    { HN: '10001', name: 'ธัญชนก', phone: '0912312312', address: 'ล.ต. 333/3 ถนนวิภาวดีรังสิต แขวงจอมพล เขตจตุจักร กรุงเทพมหานคร 10900', congenitalDisease: [{ label: 'ความดันสูง' }], gender: 'ชาย', Blood: 'a', date: '2000-12-15' },
    { HN: '10002', name: 'นันทภพ', phone: '0912312311', address: 'ล.ต. 123/33 ถนนวิภาวดีรังสิต แขวงจอมพล เขตจตุจักร กรุงเทพมหานคร 10900', congenitalDisease: [{ label: 'ความดันต่ำ' }, { label: 'นิ้วขาด' }], gender: 'ชาย', Blood: 'a', date: '2000-12-15' },
    { HN: '10003', name: 'ธาริกา', phone: '0912312111', address: '2034,2036,2038 ถนนลาดพร้าว แขวงวังทองหลาง เขตวังทองหลาง กรุงเทพ 10310', congenitalDisease: [{ label: 'หัวใจ' }], gender: 'หญิง', Blood: 'ab', date: '2000-12-15' },
]