import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { Grid, Paper } from '@material-ui/core'
import { Events, scroll, scrollSpy, Link, Element } from 'react-scroll'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import Modal from "./Modal.js"
import ScheduleProviders from './Body.js'
import ScheduleProvidersHeader from './Header.js'
const styles = theme => ({
    root: {
        flexGrow: 1
    },
    paper: {
        width: '100%'
    },
    control: {
        padding: theme.spacing.unit * 2
    },
    paper1: {
        padding: `${theme.spacing.unit * 2}px 0px`,
        textAlign: 'center',
        color: theme.palette.text.secondary,
        height: '10px'
    },
    rowAvatar: {
        display: 'flex',
        justifyContent: 'center'
    },
    chip: {
        margin: theme.spacing.unit,
        backgroundColor: '#e6e6e6',
        color: '#1d1d1d',
        border: '1px solid #e6e6e6'
    },
    button: {
        borderTopRightRadius: '25px',
        borderBottomRightRadius: '25px',
        flot: 'left'
    },
    rootMenu: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper
    }
})
const ITEM_HEIGHT = 48
class ScheduleProvidersIndex extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            listProviders: [],
            anchorEl: null,
            customer: {},
            pagi: 0,
            date: '',
            page: 0,
            rowsPerPage: 5
        }
    }
    setCustomer = (customer) => {
        this.setState({ customer })
    }
    // componentDidMount() {
    //     Events.scrollEvent.register('begin', function () {
    //         console.log('begin', arguments)
    //     })

    //     Events.scrollEvent.register('end', function () {
    //         console.log('end', arguments)
    //     })

    //     scrollSpy.update()
    // }
    // addAppointment = (appointment, id) => {
    //     console.log(
    //         [...this.state.filterRows[id].appointment.map(v => v), appointment].sort(
    //             this.compare
    //         )
    //     )
    //     let data = this.state.filterRows.map((v, key) => {
    //         if (key === id) {
    //             return {
    //                 ...v,
    //                 appointment: [...v.appointment, appointment].sort(this.compare)
    //             }
    //         }
    //         return v
    //     })
    //     this.setState({
    //         filterRows: data
    //     })
    // }
    // handleChangePage = () => {
    //     this.setState({})
    // }
    // scrollToTop = () => {
    //     scroll.scrollToTop()
    // }
    // componentWillUnmount() {
    //     Events.scrollEvent.remove('begin')
    //     Events.scrollEvent.remove('end')
    // }
    // handleClick = event => {
    //     this.setState({ anchorEl: event.currentTarget })
    // }

    // handleClose = () => {
    //     this.setState({ anchorEl: null })
    // }
    // compare = (a, b) => {
    //     if (a.start < b.start) return -1
    //     if (a.start > b.start) return 1
    //     return 0
    // }

    render() {
        const { classes, openingTime, providers, heightOfRow, openModal, handleModel } = this.props
        const { filterRows, anchorEl, page, rowsPerPage } = this.state
        console.log(providers)
        const open = Boolean(anchorEl)
        return (
            <div>
                <Grid container className={classes.root}>
                    <Grid item xs={12} style={{ margin: 'auto' }}>
                        <Grid container className={classes.Navbar} justify='center'>
                            <Paper className={classes.paper}>
                                <ScheduleProvidersHeader
                                    providers={providers.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
                                    openingHours={openingTime}
                                />
                                <Element
                                    name='test7'
                                    className='element'
                                    id='containerElement'
                                    style={{
                                        position: 'relative',
                                        height: '72vh',
                                        overflow: 'scroll',
                                        overflowX: 'hidden'
                                    }}
                                >
                                    <ScheduleProviders
                                        providers={providers.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
                                        heightOfRow={heightOfRow}
                                        openingHours={openingTime}
                                        handleModel={handleModel}
                                    />
                                </Element>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>

                <Modal openModal={openModal} handleModel={handleModel} />
            </div>
        )
    }
}

ScheduleProvidersIndex.propTypes = {
    classes: PropTypes.object.isRequired,
    openingTime: PropTypes.array.isRequired,
    providers: PropTypes.array.isRequired
}
ScheduleProvidersIndex.defultProps = {
    openingTime: [],
    providers: []
}

export default withStyles(styles)(ScheduleProvidersIndex)
