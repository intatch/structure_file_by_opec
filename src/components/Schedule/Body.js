import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Typography, Grid, Button } from "@material-ui/core";
import CardProvider from "./Card.js";
import Scroll from "react-scroll";
import { Add as AddIcon } from '@material-ui/icons'
import className from 'classname'
var Element = Scroll.Element;
const heightRow = "180px";
const styles = theme => ({
    appBar: {
        position: "relative"
    },
    flex: {
        flex: 1
    },
    root: {
        flexGrow: 1,
        justify: "center"
    },
    rowAvatar: {
        display: "flex",
        justifyContent: "center"
    },
    avatar: {
        margin: -10
    },
    paper: {
        padding: theme.spacing.unit,
        textAlign: "center",
        height: heightRow,
        color: theme.palette.text.secondary
    },
    list: {
        zIndex: "-2",

    },
    position: {
        position: "relative",
        width: "100%",
        height: "100px",
        margin: "0",
        padding: "0",
        border: "0",
        background: "rgba(0, 0, 0, 0.14)",
        top: "91%"
    },
    paper1: {
        color: theme.palette.text.secondary
    },
    colorTable: {
        '&:nth-child(even)': {
            backgroundColor: '#dbebed'
        }
    },
    fab: {
        position: 'fixed',
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 2,
    },
});

class ScheduleProviders extends React.Component {
    render() {
        const {
            classes,
            openingHours,
            providers,
            addArr,
            addAppointment,
            heightOfRow,
            handleModel
        } = this.props;

        return (
            <div >
                <Grid container style={{ padding: 0, margin: 0, height: 0 }}>
        
                    <Button variant="fab" className={classes.fab} >
                        <AddIcon />
                    </Button>
                    <Grid
                        item
                        className={classes.paper1}
                        style={{ padding: "0px", fontSize: "35px" }}
                        xs={12}
                        xl={1}
                        sm={1}
                        md={1}
                        lg={1}
                    />

                    {providers.map((provider, key1) => (
                        <Grid
                            key={provider.name + "ss"}
                            className={classes.paper1}
                            item
                            xs={12}
                            xl={2}
                            sm={2}
                            md={2}
                            lg={2}
                        >
                            <div
                                style={
                                    key1 + 1 !== providers.length
                                        ? {
                                            height: `${parseInt(heightOfRow) * openingHours.length}px`,
                                            position: "relative",
                                            borderLeftStyle: "solid",
                                            borderWidth: "0.5px"
                                        }
                                        : {
                                            borderRightStyle: "solid",
                                            height: `${parseInt(heightOfRow) * openingHours.length}px`,
                                            position: "relative",
                                            borderLeftStyle: "solid",
                                            borderWidth: "0.5px"
                                        }
                                }
                            >
                                <CardProvider
                                handleModel={handleModel}
                                    heightOfRow={heightOfRow}
                                    openingHours={openingHours}
                                    provider={provider}
                                />
                            </div>
                        </Grid>
                    ))}
                </Grid>
                {openingHours.map((Hour, key) => {
                    if (key + 1 !== openingHours.length) {
                        return (
                            <Element key={Hour + "xx"} name={Hour} className={classes.colorTable}>
                                <Grid className={classes.list} style={{ height: heightOfRow }} item xs={12}>
                                    <Grid className={classes.paper} item xs={1}>
                                        <Typography
                                            variant="title"
                                            style={{
                                                padding: `${parseInt(heightOfRow) / 3}px`,
                                                top: "50%"
                                            }}
                                        >
                                            {Hour}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Element>
                        );
                    }
                })}
            </div>
        );
    }
}

ScheduleProviders.propTypes = {
    classes: PropTypes.object.isRequired,
    openingHours: PropTypes.array.isRequired,
    providers: PropTypes.array.isRequired,
    addArr: PropTypes.func.isRequired,
    addAppointment: PropTypes.string.isRequired,
    heightOfRow: PropTypes.string.isRequired
};
ScheduleProviders.defaultProps = {
    classes: {},
    openingHours: "",
    providers: [],
    addArr: function () {
        throw new Error("func addArr not found");
    },
    addAppointment: function () {
        throw new Error("func openingHours not found");
    },
    openingHours: [],
    heightOfRow: ""
};

export default withStyles(styles)(ScheduleProviders);
