import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Grid, Chip } from "@material-ui/core";
import { ToggleButtonGroup, ToggleButton } from "@material-ui/lab";
import { Events, scroll, scrollSpy, Link, Element } from "react-scroll";
import { Face as FaceIcon, AccessTime as AccessTimeIcon, ArrowRight as ArrowRightIcon, ArrowLeft as ArrowLeftIcon } from "@material-ui/icons";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import BabyIcon from "../../assets/icon/baby.svg"// ทันตกรรมสำหรับเด็ก (Pediatric Dentistry) 
import SleepIcon from "../../assets/icon/sleep.svg"// ทันตกรรมนอนกรน 
import DentalIcon from "../../assets/icon/dental.svg"// ทันตกรรมจัดฟัน (Orthodontics)
import MaxillofacialIcon from "../../assets/icon/Maxillofacial.svg"// ศัลยกรรมช่องปากและกระดูกขากรรไกร (Oral and Maxillofacial Surgery)
import DenturesIcon from "../../assets/icon/dentures.svg"// ทันตกรรมประดิษฐ์ (ฟันปลอม)
import MolarIcon from "../../assets/icon/molar.svg"// ทันตกรรมบดเคี้ยว (Occlusion)
import EmpasteIcon from "../../assets/icon/empaste.svg"// ทันตกรรมหัตถการ
import Endodontics from "../../assets/icon/Endodontics.svg"// ทันตกรรมรักษารากฟัน (Endodontics)
import PeriodonticsIcon from "../../assets/icon/Periodontics.svg"// ทันตกรรมปริทันต์(Periodontics)
import GeneralIcon from "../../assets/icon/General.svg"// ทันตกรรมทั่วไป (General Dentistry)
import Avatar from '@material-ui/core/Avatar';
const styles = theme => ({
    root: {
        flexGrow: 1
    },
    paper: {
        width: "100%"
    },
    control: {
        padding: theme.spacing.unit * 2
    },
    paper1: {
        textAlign: "center",
        color: theme.palette.text.secondary,
        borderRight: "solid",
        borderRightWidth: "0.5px",
        padding: "16px 0"
    },
    rowAvatar: {
        display: "flex",
        margin: "auto",
        height: "100%"
    },
    chip: {
        margin: theme.spacing.unit,
        backgroundColor: "#e6e6e6",
        color: "#1d1d1d",
        border: "1px solid #e6e6e6"
    },
    button: {
        borderTopRightRadius: "25px",
        borderBottomRightRadius: "25px",
        flot: "left"
    },
    rootMenu: {
        width: "100%",
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper
    },
    header: {
        padding: 0,
        margin: 0,
        borderBottom: "0.1px solid #f4f4f4 ",
        position: "relative",
        paddingRight: "5px"
    }
});
const ITEM_HEIGHT = 48;
class ScheduleProvidersHeader extends React.Component {
    state = {
        filterRows: [],
        anchorEl: null
    };
    handleFirstPageButtonClick = event => {
        this.props.onChangePage(event, 0);
    };

    handleBackButtonClick = event => {
        this.props.onChangePage(event, this.props.page - 1);
    };

    handleNextButtonClick = event => {
        this.props.onChangePage(event, this.props.page + 1);
    };

    handleLastPageButtonClick = event => {
        this.props.onChangePage(
            event,
            Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
        );
    };
    render() {
        const {
            classes,
            openingTime,
            providers,
            count,
            page,
            rowsPerPage,
            makePagination,
            handleChangePage
        } = this.props;
        const { filterRows, anchorEl } = this.state;
        const open = Boolean(anchorEl);
        return (
            <Grid container className={classes.header}>
                <Grid item className={classes.paper1} style={{ fontSize: "35px", borderRight: "solid", borderRightWidth: "0.5px", }} xs={12} xl={1} sm={1} md={1} lg={1} >
                    <div className={classes.rowAvatar}>
                        <div className={classes.rootMenu}>
                            <AccessTimeIcon aria-label="More" aria-owns={open ? "long-menu" : null} aria-haspopup="true" onClick={this.handleClick} />
                            <Menu
                                id="long-menu"
                                anchorEl={anchorEl}
                                open={open}
                                onClose={this.handleClose}
                                PaperProps={{
                                    style: {
                                        maxHeight: ITEM_HEIGHT * 4.5,
                                        width: "10vh"
                                    }
                                }}
                            >
                                {openingTime.map(option => (
                                    <MenuItem style={{ margin: 0, padding: 0 }} key={option} selected={option === "Pyxis"}>
                                        <Link
                                            style={{ width: "100%" }}
                                            onClick={this.handleClose}
                                            activeClass="active"
                                            to={option}
                                            spy={true}
                                            smooth={true}
                                            duration={1950}
                                            containerId="containerElement"
                                        >
                                            {option}
                                        </Link>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </div>
                    </div>
                </Grid>
                {
                    providers.map(v => (
                        <Grid key={v.name} item className={classes.paper1} xs={12} xl={2} sm={2} md={2} lg={2} >
                            <div className={classes.rowAvatar} >
                                <div style={{ margin: "auto", }}>
                                    {v.department === "Baby" && <img src={BabyIcon} height='35px' />}
                                    {v.department === "Sleep" && <img src={SleepIcon} height='35px' />}
                                    {v.department === "Dental" && <img src={DentalIcon} height='35px' />}
                                    {v.department === "Maxillofacial" && <img src={MaxillofacialIcon} style={{ color: "#000" }} height='35px' />}
                                    {v.department === "Dentures" && <img src={DenturesIcon} height='35px' />}
                                    {v.department === "Molar" && <img src={MolarIcon} height='35px' />}
                                    {v.department === "Empaste" && <img src={EmpasteIcon} height='35px' />}
                                    {v.department === "Endodon" && <img src={Endodontics} height='35px' />}
                                    {v.department === "Periodontics" && <img src={PeriodonticsIcon} height='35px' />}
                                    {v.department === "General" && <img src={GeneralIcon} height='35px' />}
                                </div>
                                <div style={{ margin: "auto" }}>
                                    {v.name}
                                </div>
                            </div>
                        </Grid>
                    ))
                }
                <Grid item xs={12} xl={1} sm={1} md={1} lg={1} className={classes.paper1}>
                    <ToggleButtonGroup
                        exclusive
                        onChange={this.handleAlignment}
                        style={{
                            height: "100%",
                            margin: "auto"
                        }}
                    >
                        <ToggleButton
                            style={{
                                width: "20%",
                                background: "#e6e6e6",
                                borderTopLeftRadius: "16px",
                                borderBottomLeftRadius: "16px"
                            }}
                            value=""
                            onClick={() =>
                                this.makePagination(this.state.fullRows, 4, this.state.pagi - 1)
                            }
                        >
                            <ArrowLeftIcon />
                        </ToggleButton>
                        <ToggleButton
                            value=""
                            style={{
                                width: "20%",
                                background: "#e6e6e6",
                                borderBottomRightRadius: "16px",
                                borderTopRightRadius: "16px"
                            }}
                            onClick={() =>
                                makePagination(this.state.fullRows, 4, this.state.pagi + 1)
                            }
                        >
                            <ArrowRightIcon />
                        </ToggleButton>
                    </ToggleButtonGroup>
                </Grid>
            </Grid >
        );
    }
}

ScheduleProvidersHeader.propTypes = {
    classes: PropTypes.object.isRequired
};

ScheduleProvidersHeader.defaultProps = {
    openingTime: []
};
export default withStyles(styles)(ScheduleProvidersHeader);
