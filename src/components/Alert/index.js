import React, { Component } from 'react';
import { withSwalInstance } from 'sweetalert2-react';
import swal from 'sweetalert2';

const SweetAlert = withSwalInstance(swal);

export default class Swal extends Component {
    render() {
        const { show, title, text, onConfirm } = this.props
        return (
            <SweetAlert
                show={show}
                title={title}
                text={text}
                onConfirm={onConfirm}
            />
        );
    }
}