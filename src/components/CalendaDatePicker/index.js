import React, { Component } from 'react';
import { withStyles } from '@material-ui/core'
import DatePicker from "./DatePicker.js"
import TextField from '@material-ui/core/TextField'
import Menu from '@material-ui/core/Menu'
import dateFns from "date-fns"
import PropTypes from 'prop-types';
const styles = theme => ({
    Menu: {
        padding: 0
    }
})
class Input extends Component {
    state = {
        anchorEl: null,
        currentMonth: new Date(),
        selectedDate: new Date(2000),
        events: { [new Date().setHours(0, 0, 0, 0)]: 'empty' }
    }
    onDateClick = day => {
        this.setState({
            selectedDate: day,
            anchorEl: null
        })
    }

    nextMonth = () => {
        this.setState({
            currentMonth: dateFns.addMonths(this.state.currentMonth, 1)
        })
    }

    prevMonth = () => {
        this.setState({
            currentMonth: dateFns.subMonths(this.state.currentMonth, 1)
        })
    }
    handleClickListItem = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleMenuItemClick = (event, index) => {
        this.setState({ selectedIndex: index, anchorEl: null });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    }
    render() {
        const { classes } = this.props;
        const { anchorEl } = this.state;
        return (
            <div>
                <TextField
                    fullWidth
                    id="standard-Date"
                    label="Date"
                    margin="Date"
                    onClick={this.handleClickListItem}
                    value={dateFns.format(this.state.selectedDate, 'D/M/YYYY')}
                />
                <Menu
                    id="lock-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                    className={classes.Menu}
                >
                    <DatePicker {...this.props}
                        onDateClick={this.onDateClick}
                        events={this.state.events}
                        nextMonth={this.nextMonth}
                        prevMonth={this.prevMonth}
                        currentMonth={this.state.currentMonth}
                        selectedDate={this.state.selectedDate}
                    />
                </Menu>
            </div>
        );
    }
}
export default withStyles(styles)(Input)