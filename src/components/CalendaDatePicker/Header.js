import React from 'react';
import { withStyles } from '@material-ui/core'
import { KeyboardArrowLeft, KeyboardArrowRight } from "@material-ui/icons"
import classNames from 'classname'
import dateFns from 'date-fns'
const styles = theme => ({
    header: {
        display: "block",
        width: "100%",
        borderBottom: "1px solid #eee",
        background: "",
        display: "flex",
        padding: "1.5em 0"
    },
    row: {
        borderBottom: '1px solid #eee'
    },
    col: {
        flexGrow: '1',
        fleBasis: '0',
        maxWidth: '100%',
    },
    colStart: {
        justifyContent: 'flex-start',
        textAlign: 'left'
    },
    icon: {
        cursor: 'pointer',
        transition: '0.15s ease-out',
        '&hover': {
            transform: 'scale(1.75)',
            transition: '0.25s ease-out',
            color: '#1a8fff',
        }
    },
    colCenter: {
        justifyContent: 'center',
        textAlign: 'center'
    },
    colEnd: {
        justifyContent: 'flex-end',
        textAlign: 'right'
    }
})
const Header = (props) => {
    const dateFormat = "MMMM YYYY"
    const { classes, prevMonth, currentMonth, nextMonth } = props
    return (
        <div className={classNames(classes.header, classes.row)} >
            <div className={classNames(classes.col, classes.colStart)} >
                <div className={classes.icon} onClick={prevMonth}>
                    <KeyboardArrowLeft />
                </div>
            </div>
            <div className={classNames(classes.col, classes.colCenter)}>
                <span>{dateFns.format(currentMonth, dateFormat)}</span>
            </div>
            <div className={classNames(classes.col, classes.colEnd)} onClick={nextMonth}>
                <div className={classes.icon}>
                    <KeyboardArrowRight />
                </div>
            </div>
        </div>
    )
}

export default withStyles(styles)(Header)