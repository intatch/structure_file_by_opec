import React from 'react'
import { withStyles } from "@material-ui/core/styles"
import dateFns from 'date-fns'
import Classname from 'classname'
const style = theme => ({
    dey: {
        boxSizing: "border-box",

    },
    col: {
        flexGrow: '1',
        flexBasis: '0',
        maxWidth: '100%',
    },
    colCenter: {
        justifyContent: 'center',
        textAlign: 'center',
    },
    row: {
        margin: '0',
        padding: '0',
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        display: 'flex'
    }
})
const Days = props => {
    const { currentMonth, classes } = props
    const dateFormat = "ddd"
    const days = []

    let startDate = dateFns.startOfWeek(currentMonth)

    for (let i = 0; i < 7; i++) {
        days.push(
            <div className={Classname(classes.col, classes.colCenter)} key={i}>
                {dateFns.format(dateFns.addDays(startDate, i), dateFormat)}
            </div>
        )
    }

    return <div className={Classname(classes.days, classes.row)}> {days}</div >
}

export default withStyles(style)(Days)