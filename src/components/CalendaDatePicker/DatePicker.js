import React from "react"
import PropTypes from 'prop-types';

import { withStyles } from "@material-ui/core/styles"
import Header from './Header.js'
import Cells from './Cells.js'
import Day from './Day.js'
const style = thmeme => ({
    calendar: {
        boxSizing: "border-box",
        fontSize: "1em",
        fontWeight: "300",
        lineHeight: "1.5",
        color: "#777",
        background: "#f9f9f9",
        position: " relative",
        display: 'block',
        width: '100%',
        border: '1px solid #eee',
    }
})
class DatePicker extends React.Component {

    render() {
        const { classes, nextMonth, prevMonth, onDateClick, events } = this.props
        return (
            <div className={classes.calendar} >
                <Header
                    nextMonth={nextMonth}
                    prevMonth={prevMonth}
                    currentMonth={this.props.currentMonth}
                    selectedDate={this.props.selectedDate}
                />
                <Day

                    currentMonth={this.props.currentMonth}
                    selectedDate={this.props.selectedDate} />
                <Cells
                    events={events}
                    onDateClick={onDateClick}
                    currentMonth={this.props.currentMonth}
                    selectedDate={this.props.selectedDate} />
            </div>
        )
    }
}
DatePicker.propTypes = {
    classes: PropTypes.object.isRequire,
    nextMonth: PropTypes.func.isRequire,
    prevMonth: PropTypes.func.isRequire,
    onDateClick: PropTypes.func.isRequire,
    events: PropTypes.object.isRequire
}
DatePicker.defaultProps = {
    classes: {},
    nextMonth: function () {
        console.log(`not found nextMonth`)
    },
    prevMonth: function () {
        console.log(`not found prevMonth`)
    },
    onDateClick: function () {
        console.log(`not found onDateClick`)
    },
    events: {}
}
export default withStyles(style)(DatePicker)

