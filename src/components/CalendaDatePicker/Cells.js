import React from 'react'
import { withStyles } from '@material-ui/core'
import classNames from 'classname'
import dateFns from 'date-fns'
const styles = theme => ({
    col: {
        flexGrow: '1',
        flexBasis: '0',
        maxWidth: '100%'
    },
    cell: {
        height: '50px',
        width: '50px',
        overflow: 'hidden',
        cursor: 'pointer',
        transition: '0.25s ease-out',
        overFrow: "auto",
        boxSizing: "border-box",
        textAlign: '-webkit-center',
        position: 'sticky'
    },
    disabled: {
        color: '#ccc',
        pointerEvents: 'none'
    },
    selected: {
        borderRadius: '50px',
        backgroundColor: '#bbb'
    },
    number: {
        fontSize: '200%',
        top: '0',
        right: '0',
        fontWeight: '700',
        borderRadius: '50px',

    },
    bg: {
        fontWeight: 700,
        color: '#1a8fff',
        opacity: 0,
        fontSize: '800%',
        position: 'absolute',
        top: '-2px',
        right: '0.03em',
        transition: "0.25s ease-out",
        letterSpacing: '-0.07em',
    },
    row: {
        margin: 0,
        padding: 0,
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        display: '-webkit-box',
    },
    body: {
        fontSize: '1em',
        fontWeight: 300,
        lineHeight: 1.5,
        color: '#4d4848bd',
        background: '#f9f9f9',
        position: 'relative'
    },
    dot: {
        height: '100%',
        width: '100%',
        zIndex: '-1',
        borderRadius: '25%',
        display: 'inline-block',
        margin: 0,
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
    empty: {
        backgroundColor: '#A7DBEA',
    },
    full: {
        backgroundColor: '#FE8B8B',
    },
    haveADate: {
        backgroundColor: '#FEF78B',
    }
})
const Cells = props => {
    const { currentMonth, selectedDate, classes, events } = props
    const monthStart = dateFns.startOfMonth(currentMonth)
    const monthEnd = dateFns.endOfMonth(monthStart)
    const startDate = dateFns.startOfWeek(monthStart)
    const endDate = dateFns.endOfWeek(monthEnd)

    const dateFormat = "D"
    const rows = []

    let days = []
    let day = startDate
    let formattedDate = ""

    while (day <= endDate) {
        for (let i = 0; i < 7; i++) {
            formattedDate = dateFns.format(day, dateFormat)
            const cloneDay = day
            const event = events[day.setHours(0, 0, 0, 0)]
            days.push(
                <div
                    className={classNames(classes.col, classes.cell)}
                    key={day}
                >
                    {!dateFns.isSameMonth(day, monthStart) || dateFns.isBefore(day, dateFns.subDays(new Date(), 1)) ?
                        <div
                            className={classNames(classes.col, classes.cell, !dateFns.isSameMonth(day, monthStart)
                                ? classes.disabled
                                : dateFns.isSameDay(day, selectedDate)
                                    ? classes.selected
                                    : "")}
                            key={day}
                        >
                        </div>
                        :
                        <div
                            className={classNames(classes.col, classes.cell, !event ? null
                                : event === 'empty' ? classes.empty
                                    : event === 'full' ? classes.full
                                        : event === 'haveADate' && classes.haveADate)}
                            key={day}
                            onClick={() => props.onDateClick(dateFns.parse(cloneDay))}
                        >
                            <span className={classes.number}>{formattedDate}</span>
                            <spen className={classNames(classes.dot, !dateFns.isSameMonth(day, monthStart)
                                ? classes.disabled
                                : dateFns.isSameDay(day, selectedDate)
                                    ? classes.selected
                                    : "")}></spen>

                        </div>
                    }

                </div>
            )
            day = dateFns.addDays(day, 1)
        }
        rows.push(
            <div className={classes.row} key={day}>
                {days}
            </div>
        )
        days = []
    }
    return <div className={classes.body}>{rows}</div>
}

export default withStyles(styles)(Cells)