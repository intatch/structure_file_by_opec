import React from 'react';
import PropTypes from 'prop-types';
import deburr from 'lodash/deburr';
import keycode from 'keycode';
import Downshift from 'downshift';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Popper from '@material-ui/core/Popper';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar'
function renderInput(inputProps) {
    const { InputProps, classes, ref, ...other } = inputProps;

    return (
        <TextField
            InputProps={{
                inputRef: ref,
                classes: {
                    root: classes.inputRoot,
                    input: classes.inputInput,
                },
                ...InputProps,
            }}
            {...other}
        />
    );
}

function renderSuggestion({ suggestion, index, itemProps, highlightedIndex, selectedItem }) {
    const isHighlighted = highlightedIndex === index;
    const isSelected = (selectedItem || '').indexOf(suggestion.label) > -1;
    return (
        <MenuItem
            {...itemProps}
            key={suggestion.label}
            selected={isHighlighted}
            component="div"
            style={{
                fontWeight: isSelected ? 500 : 400,
            }}
        >
            {suggestion.label}
        </MenuItem>
    );
}
renderSuggestion.propTypes = {
    highlightedIndex: PropTypes.number,
    index: PropTypes.number,
    itemProps: PropTypes.object,
    selectedItem: PropTypes.string,
    suggestion: PropTypes.shape({ label: PropTypes.string }).isRequired,
};


class DownshiftMultiple extends React.Component {
    state = {
        inputValue: '',
        selectedItem: [],
        isOpen2: false
    };
    getSuggestions = (value, lists) => {
        const inputValue = deburr(value.trim()).toLowerCase();
        const inputLength = inputValue.length;
        return lists.filter(suggestion => {
            const keep = suggestion.label.toLowerCase().match(value.toLowerCase()) !== null;
            return keep;
        });
    }


    handleKeyDown = event => {
        const { inputValue, selectedItem } = this.state;
        if (selectedItem.length && !inputValue.length && keycode(event) === 'backspace') {
            this.setState({
                selectedItem: selectedItem.slice(0, selectedItem.length - 1),
            });
        }
    };

    handleInputChange = event => {
        this.setState({ inputValue: event.target.value });
    };

    handleChange = item => {
        let { selectedItem } = this.state;
        let data = selectedItem.filter(e => item.value === e.value)
        if (data.length === 0) {
            this.setState({
                inputValue: '',
                selectedItem: [...selectedItem, { ...item, amont: 1 }],
            });
        } else {
            this.setState({
                inputValue: '',
                selectedItem: selectedItem.map(value => {
                    if (value.value === item.value) {
                        return { ...value, amont: value.amont + 1 }
                    }
                    return value
                }),
            });
        }

    };

    handleDelete = item => () => {
        this.setState(state => {
            return { selectedItem: state.selectedItem.filter(v => item !== v.value) };
        });
    };
    handlePlus = item => () => {
        this.setState(state => {
            return {
                selectedItem: state.selectedItem.map((v, key) => {
                    if (item === v.label) {
                        return { ...v, amont: v.amont + 1 }
                    }
                    return v
                })
            };
        });
    };
    openMennu = () => {
        this.setState(state => {
            return { isOpen2: !state.isOpen2 }
        })
    }
    render() {
        const { classes, suggestions ,label} = this.props;
        const { inputValue, selectedItem, isOpen2 } = this.state;

        return (
            <Downshift
                id="downshift-multiple"
                inputValue={inputValue}
                onChange={this.handleChange}
                selectedItem={selectedItem}
            >
                {({
                    getInputProps,
                    getItemProps,
                    isOpen,
                    inputValue: inputValue2,
                    selectedItem: selectedItem2,
                    highlightedIndex,
                }) => (
                        <div className={classes.container}>
                            {renderInput({
                                fullWidth: true,
                                classes,
                                InputProps: getInputProps({
                                    startAdornment: selectedItem.map(item => (
                                        <Chip
                                            key={item.label}
                                            tabIndex={-1}
                                            label={item.label}
                                            className={classes.chip}
                                            avatar={<Avatar onClick={this.handlePlus(item.label)}>{item.amont}</Avatar>}
                                            onDelete={this.handleDelete(item.value)}
                                        />
                                    )),
                                    onChange: this.handleInputChange,
                                    onKeyDown: this.handleKeyDown,
                                    onFocus: this.openMennu,
                                    onBlur: this.openMennu,
                                    placeholder: `Select ${label}`,
                                }),
                                label,
                            })}
                            {isOpen2 ? (
                                <Paper className={classes.paper} square>
                                    {this.getSuggestions(inputValue2, suggestions).map((suggestion, index) =>
                                        renderSuggestion({
                                            suggestion,
                                            index,
                                            itemProps: getItemProps({ item: suggestion }),
                                            highlightedIndex,
                                            selectedItem: selectedItem2,
                                        }),
                                    )}
                                </Paper>
                            ) : null}
                        </div>
                    )}
            </Downshift>
        );
    }
}

DownshiftMultiple.propTypes = {
    classes: PropTypes.object.isRequired,
    suggestions: PropTypes.array.isRequired,
    label: PropTypes.string.isRequired
};
DownshiftMultiple.defaultProps = {
    classes: {},
    suggestions: [],
    label: ''
}
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    container: {
        flexGrow: 1,
        position: 'relative',
    },
    paper: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0,
        maxHeight: '30vh',
        overflow: 'auto'
    },
    chip: {
        margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
    },
    inputRoot: {
        flexWrap: 'wrap',
    },
    inputInput: {
        width: 'auto',
        flexGrow: 1,
    },
    divider: {
        height: theme.spacing.unit * 2,
    },
});

function IntegrationDownshift(props) {
    const { classes, suggestions,label } = props;

    return (
        <div className={classes.root}>
            <DownshiftMultiple label={label} suggestions={suggestions} classes={classes} />
        </div>
    );
}

IntegrationDownshift.propTypes = {
    classes: PropTypes.object.isRequired,
    suggestions: PropTypes.array.isRequired,
    label: PropTypes.string.isRequired
};
IntegrationDownshift.defaultProps = {
    classes: {},
    suggestions: [],
    label: ""
}
export default withStyles(styles)(IntegrationDownshift);
