import React, { Component } from 'react';
import { Drawer, IconButton, List, ListItem, ListItemIcon } from '@material-ui/core'
import { ChevronLeft as ChevronLeftIcon, ChevronRight as ChevronRightIcon } from '@material-ui/icon'
import { withStyles } from '@material-ui/core/styles'
const drawerWidth = 240
const styles = theme => ({
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        height: '7vh',
        ...theme.mixins.toolbar,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth
    },
})
class Drawer extends Component {
    render() {
        const { menus, handleDrawerClose } = this.props
        return (
            <Drawer
                variant='persistent'
                anchor='left'
                open={open}
                classes={{
                    paper: classes.drawerPaper
                }}
            >
                <div className={classes.drawerHeader}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'rtl' ?
                            (
                                <ChevronRightIcon />
                            ) :
                            (
                                <ChevronLeftIcon />
                            )
                        }
                    </IconButton>
                </div>
                <List component='nav'>
                    {menus.map((menu, key => {
                        return (
                            <ListItem key={menu.name} component={props => <Link to={MenuItem.link} {...props} />} button>
                                <ListItemIcon>
                                    <img src={menu.icon} height='35px' />
                                </ListItemIcon>
                                <ListItemText primary={menu.name} />
                            </ListItem>
                        )
                    }))}
                </List>
            </Drawer>
        );
    }
}
export default withStyles(styles, { withTheme: true })(Drawer)

// {name: "", 
// link: "/user",
// icon: ""  // 
// }