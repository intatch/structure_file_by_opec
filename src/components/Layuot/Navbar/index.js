import React, { Component } from 'react'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import { AppBar, Toolbar, Typography, IconButton } from '@material-ui/core'
const styles = theme => ({
    menuButton: {
        marginLeft: 12,
        marginRight: 20
    },
    hide: {
        display: 'none'
    },
    grow: {
        width: '5%',
        display: 'flex',
        color: '#3fa9f5'
    },
    appBar: {
        backgroundColor: '#EFEFEF',
        color: '#000'
    },
    logo: {
        marginRight: '1%'
    }
})
class Navbar extends Component {
    render() {
        const { handleDrawerOpen, classes } = this.porps
        return (
            <AppBar
                className={classNames(classes.appBar, {
                    [classes.appBarShift]: open,
                    [classes[`appBarShift-${anchor}`]]: open
                })}
                position='static'
            >
                <Toolbar disableGutters={!open}>
                    <IconButton
                        color='inherit'
                        aria-label='Open drawer'
                        className={classNames(classes.menuButton, open && classes.hide)}
                    >
                        <img onClick={handleDrawerOpen} src='http://blueseas.co.th/img/logo.png' className={classes.logo} />
                    </IconButton>
                    <Typography variant='h6' className={classes.grow}>
                        BlueSeas
                </Typography>
                </Toolbar>
            </AppBar>
        )
    }
}
export default withStyles(styles, { withTheme: true })(Navbar)