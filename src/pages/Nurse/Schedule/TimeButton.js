import React, { PureComponent } from "react"
import PropTypes from "prop-types"
import { withStyles } from "@material-ui/core/styles"
import ToggleButton from "@material-ui/lab/ToggleButton"
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup"
import Grid from '@material-ui/core/Grid'
import moment from "moment"
const styles = theme => ({
    root: {
        flexGrow: 1
    },
    toggleContainer: {
        height: 56,
        padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-start",
        margin: `16px 6px 0px`,
    }
})

class ToggleButtons extends PureComponent {
    state = {
        alignment: 0,
        day: []
    }
    handleAlignment = (event, alignment) => {
        if (alignment === this.state.alignment) {
            return
        } else {
            // this.props.handleSetTimeShow(alignment)
            this.setState({
                alignment: alignment,
                day: [
                    moment(alignment).add(-2, "days"),
                    moment(alignment).add(-1, "days"),
                    alignment,
                    moment(alignment).add(1, "days"),
                    moment(alignment).add(2, "days")
                ]
            })
        }
    }
    componentDidMount() {
        if (typeof this.props.date === "string") {
            this.setTime(new Date(this.props.date))
        } else {
            this.setTime(this.props.date)
        }
    }
    setTime = async data => {
        let today = moment(data)
        console.log(data)
        await this.setState({
            alignment: today,
            day: [
                moment(data).add(-2, "days"),
                moment(data).add(-1, "days"),
                today,
                moment(data).add(1, "days"),
                moment(data).add(2, "days")
            ]
        })
    }
    // componentDidUpdate() {
    //   this.setTime(this.props.date)
    // }
    render() {
        const { classes, forDate, } = this.props
        const { alignment } = this.state

        return (
            <Grid container>
                <Grid item xs={12}>
                    <div className={classes.toggleContainer}>
                        <ToggleButtonGroup
                            value={alignment}
                            exclusive
                            onChange={this.handleAlignment}
                            style={{
                                height: "100%",
                                width: "100%",
                                borderTopLeftRadius: "10px",
                                borderBottomLeftRadius: "10px",
                                borderBottomRightRadius: "10px",
                                borderTopRightRadius: "10px"
                            }}
                        >
                            {this.state.day.map((v, key) => {
                                {
                                    if (key === 0) {
                                        return (<ToggleButton
                                            key={v}
                                            style={{
                                                height: "100%", width: "20%", background: "#a3d8e5"
                                            }}
                                            value={v}
                                            onClick={() => {
                                                this.setTime(v)
                                                forDate()
                                            }}
                                            disabled={v === this.state.alignment}
                                        >
                                            {`${v.format("MMMM Do YY")}`}
                                        </ToggleButton>)
                                    } else if (this.state.alignment === v) {
                                        return (<ToggleButton
                                            key={v}
                                            style={{
                                                height: "100%", width: "20%", background: "#3fa9f5", borderLeftStyle: "solid", color: '#000'
                                            }}
                                            value={v + "TimeButton"}
                                            onClick={() => {
                                                this.setTime(v)
                                                forDate()
                                            }}
                                            disabled={v === this.state.alignment}
                                        >
                                            {`${v.format("MMMM Do YY")}`}
                                        </ToggleButton>)
                                    } else {
                                        return (<ToggleButton
                                            key={v}
                                            style={{ height: "100%", width: "20%", background: "#a3d8e5", borderLeftStyle: "solid", borderWidth: "0.5px" }}
                                            value={v + "TimeButton"}
                                            onClick={() => {
                                                this.setTime(v)
                                                forDate()
                                            }}
                                            disabled={v === this.state.alignment}
                                        >
                                            {`${v.format("MMMM Do YY")}`}
                                        </ToggleButton>)
                                    }
                                }
                            })}
                        </ToggleButtonGroup>
                    </div >
                </Grid>
            </Grid>

        )
    }
}

ToggleButtons.propTypes = {
    classes: PropTypes.object.isRequired
}

export default withStyles(styles)(ToggleButtons)
