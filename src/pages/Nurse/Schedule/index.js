import React, { Component } from 'react';
import moment from "moment";
import Schedule from '../../../components/Schedule'
import NavbarSearch from './NavbarSearch'
import Search from './Search'
import TimeButton from './TimeButton'
import Form from './Form.js'
// function makeTime(start, end, timeScope) {
//     let appointments = [];
//     start = moment(`2014-11-11 ${start}:00`);
//     end = moment(`2014-11-11 ${end}:00`);
//     let date = end.diff(start, "minute") / timeScope;
//     for (let i = 0; i < date; i++) {
//         let date = timeScope * i
//         if (i === date - 1) {
//             appointments.push({
//                 minute: start.add(date, "minute").format(`HH:mm`),
//                 timeScope: timeScope,
//                 customerName: "",
//                 type: "empty",
//                 insert: {}
//             })
//         } else if (i === 0) {
//             appointments.push({
//                 minute: start.add(0, "minute").format(`HH:mm`),
//                 timeScope: timeScope,
//                 customerName: "",
//                 type: "empty",
//                 insert: {}
//             })
//         } else {
//             appointments.push({
//                 minute: start.add(timeScope = timeScope, "minute").format(`HH:mm`),
//                 timeScope: timeScope,
//                 customerName: "",
//                 type: "empty",
//                 insert: {}
//             })
//         }
//     }
//     return appointments
// }
// const openingTime = [ // config .env
//     "08:00",
//     "09:00",
//     "10:00",
//     "11:00",
//     "12:00",
//     "13:00",
//     "14:00",
//     "15:00",
//     "16:00",
//     "17:00",
//     "18:00",
//     "19:00",
//     "20:00",
//     "21:00"
// ];

// const providers = [
//     {
//         name: "ทญ.สุนทรี  ตั้งจาตุรนต์รัศมี",
//         start: "08:00",
//         end: "15:00",
//         appointments: makeTime("08:00", "15:00", 15),
//         type: "",
//         department: "General"
//     },
//     {
//         name: "ทญ.เสาวนีย์  แสงทองศรีกมล",
//         start: "09:00",
//         end: "16:30",
//         appointments: makeTime("09:00", "16:30", 15),
//         type: "",
//         department: "General"
//     },
//     {
//         name: "ทญ.อารีรัตน์  เพชรอินทร",
//         start: "08:30",
//         end: "16:30",
//         appointments: makeTime("08:30", "16:30", 15),
//         type: "",
//         department: "General"
//     },
//     {
//         name: "ทญ.ศุภศรี เกษมสันต์ ณ อยุธยา",
//         start: "08:00",
//         end: "16:30",
//         appointments: makeTime("08:00", "16:30", 15),
//         type: "",
//         department: "Maxillofacial"
//     },
//     {
//         name: "ทพ.วัลลภ  ภูวพานิช",
//         start: "16:30",
//         end: "20:00",
//         appointments: makeTime("16:30", "20:00", 15),
//         type: "",
//         department: "Baby"
//     }
// ];

class SchedulePage extends Component {
    state = {
        openModal: false,
        providers: [],
        step: 0
    }
    handleModel = () => {
        this.setState({ openModal: !this.state.openModal })
    }
    componentDidMount() {
        // this.setState({ providers: providers })
    }
    render() {
        const { openModal, step } = this.state
        return (
            <div>
                <NavbarSearch handleModel={this.handleModel} />
                {step === 0 &&
                    <div>
                        <Search />
                    </div>}
                {step === 1 &&
                    <div>
                        {/* <Form providers={this.state.providers} /> */}
                        {/* <Schedule
                            openModal={openModal}
                            openingTime={openingTime}
                            heightOfRow={"200px"}
                            providers={this.state.providers}
                            handleModel={this.handleModel}
                        /> */}
                    </div>}

            </div>
        );
    }
}

export default SchedulePage

