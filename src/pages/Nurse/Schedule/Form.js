import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Badge from '@material-ui/core/Badge'
import { Clear as ClearIcon } from '@material-ui/icons'
import BabyIcon from "../../../assets/icon/baby.svg"// ทันตกรรมสำหรับเด็ก (Pediatric Dentistry) 
import SleepIcon from "../../../assets/icon/sleep.svg"// ทันตกรรมนอนกรน 
import DentalIcon from "../../../assets/icon/dental.svg"// ทันตกรรมจัดฟัน (Orthodontics)
import MaxillofacialIcon from "../../../assets/icon/Maxillofacial.svg"// ศัลยกรรมช่องปากและกระดูกขากรรไกร (Oral and Maxillofacial Surgery)
import DenturesIcon from "../../../assets/icon/dentures.svg"// ทันตกรรมประดิษฐ์ (ฟันปลอม)
import MolarIcon from "../../../assets/icon/molar.svg"// ทันตกรรมบดเคี้ยว (Occlusion)
import EmpasteIcon from "../../../assets/icon/empaste.svg"// ทันตกรรมหัตถการ
import Endodontics from "../../../assets/icon/Endodontics.svg"// ทันตกรรมรักษารากฟัน (Endodontics)
import PeriodonticsIcon from "../../../assets/icon/Periodontics.svg"// ทันตกรรมปริทันต์(Periodontics)
import GeneralIcon from "../../../assets/icon/General.svg"// ทันตกรรมทั่วไป (General Dentistry)
const styles = theme => ({
    root: {
        flexGrow: 1,
        padding: theme.spacing.unit * 2,
        // display: '-webkit-inline-box'
    },
    Paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    clear: {
        top: '0',
        right: '0',
        width: '22px',
        height: '22px',
        display: 'flex',
        zIndex: '1',
        position: 'absolute',
        flexWrap: 'wrap',
        fontSize: '0.75rem',
        alignItems: 'center',
        alignContent: 'center',
        borderRadius: '50%',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    Badge: {

    }
})

class CenteredGrid extends React.Component {
    state = {
        lists: [
            { name: "Baby", amount: 0 },
            { name: "Sleep", amount: 0 },
            { name: "Dental", amount: 0 },
            { name: "Maxillofacial", amount: 0 },
            { name: "Dentures", amount: 0 },
            { name: "Molar", amount: 0 },
            { name: "Empaste", amount: 0 },
            { name: "Endodon", amount: 0 },
            { name: "Periodontics", amount: 0 },
            { name: "General", amount: 0 },]
    }
    handleRemove = (id) => {
        this.setState({ lists: this.state.lists.filter((v, key) => key !== id) })
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (JSON.stringify(nextProps.providers) !== JSON.stringify(this.props.providers)) {
            return true
        }
        return false
    }
    componentDidUpdate() {
        this.setState({ lists: this.filterDepartment() })
        // console.log(this.props.providers)
    }
    filterDepartment = () => {
        let lists = []
        let makeArr = this.props.providers.map(provider => provider.department).reduce((provider, lists) => {
            provider[lists] = (provider[lists] || 0) + 1;
            return provider
        }, {})
        for (const key in makeArr) {
            lists.push({ name: key, amount: makeArr[key] })
        }
        return lists
    }
    render() {
        const { classes } = this.props

        return (
            <div className={classes.root}>
                <Grid container xs={12} justify="center">
                    <Grid justify="center" container xs={12} spacing={16} className={classes.root}>
                        {this.state.lists.map((list, key) =>
                            <Grid item xs={1}>
                                <Badge className={classes.Badge} badgeContent={list.amount} color="secondary">
                                    <Paper className={classes.Paper} elevation={1}>
                                        {list.name === "Baby" && <img src={BabyIcon} height='40px' />}
                                        {list.name === "Sleep" && <img src={SleepIcon} height='40px' />}
                                        {list.name === "Dental" && <img src={DentalIcon} height='40px' />}
                                        {list.name === "Maxillofacial" && <img src={MaxillofacialIcon} style={{ color: "#000" }} height='40px' />}
                                        {list.name === "Dentures" && <img src={DenturesIcon} height='40px' />}
                                        {list.name === "Molar" && <img src={MolarIcon} height='40px' />}
                                        {list.name === "Empaste" && <img src={EmpasteIcon} height='40px' />}
                                        {list.name === "Endodon" && <img src={Endodontics} height='40px' />}
                                        {list.name === "Periodontics" && <img src={PeriodonticsIcon} height='40px' />}
                                        {list.name === "General" && <img src={GeneralIcon} height='40px' />}
                                        <ClearIcon className={classes.clear} onClick={() => this.handleRemove(key)} />
                                    </Paper>
                                </Badge>
                            </Grid>
                        )}
                    </Grid>
                </Grid>
            </div>
        )
    }
}

CenteredGrid.propTypes = {
    classes: PropTypes.object.isRequired,
    providers: PropTypes.array.isRequired
}

CenteredGrid.defaultProps = {
    classes: {},
    providers: []
}

export default withStyles(styles)(CenteredGrid)