import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField'
import CalendaDatePicker from '../../../components/CalendaDatePicker'
import InputMunti from '../../../components/form/inputMunti'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import { Add as AddIcon, Remove as RemoveIcon } from "@material-ui/icons"
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import InputAdornment from '@material-ui/core/InputAdornment';
import AccountCircle from '@material-ui/icons/AccountCircle'
import CalendarToday from '@material-ui/icons/CalendarToday'
import Star from '@material-ui/icons/Star'
import Dentist from "../../../assets/icon/dentist.svg"
import Tooth from '../../../assets/icon/tooth.svg'
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    textField: {
        margin: 0
    }
});

class Search extends Component {
    state = {
        HN: "",
        deparments: [],
        providers: [],
        events: {},
        selectedDate: ''
    }
    componentDidMount() {

    }
    handleChange = name => value => {
        this.setState({ [name]: value })
        // fetch
        if(name === 'deparment' || name === 'providers'){
            return 
        } else if (name === 'HN') {
            return 
        }
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Grid container alignItems="center">
                    <Grid item xs={6}>
                        <Paper className={classes.paper}>
                        <Grid container spacing={8} alignItems="flex-end">
                          <Grid item >
                            <AccountCircle />
                          </Grid>
                          <Grid item >
                          <TextField
                                id="standard-HN"
                                fullWidth
                                label="HN"
                                className={classes.textField}
                                value={this.state.name}
                                onChange={this.handleChange('HN')}
                                margin="normal"
                            />
                          </Grid>
                        </Grid>
                        <Grid container spacing={8} alignItems="flex-end">
                          <Grid item>
                          <img src={Dentist} height='24px' />
                          </Grid>
                          <Grid item>
                          <InputMunti label={`providers`} handleChange={this.handleChange('providers')} value={this.state.providers} disableUnderline={false} placeholder="Symptom" suggestions={[
                                { value: 1, label: "ทญ.สุนทรี  ตั้งจาตุรนต์รัศมี" },
                                { value: 2, label: "ทญ.เสาวนีย์  แสงทองศรีกมล" },
                                { value: 3, label: "ทญ.อารีรัตน์  เพชรอินท" },
                                { value: 4, label: "ทญ.ศุภศรี เกษมสันต์ ณ อยุธยา" },
                                { value: 5, label: "ทพ.วัลลภ  ภูวพานิช" },
                                { value: 6, label: "ทพ.ปกป้อง  อมรวิทย์" },
                                { value: 7, label: "ทพ.วิชชุพล  รัตนมาลัย" },
                                { value: 8, label: "ทญ.ชิตวรี ลีตระกูลวรรณา" },
                                { value: 9, label: "ทญ.ศีจิต เลาหะคามิน" },
                                { value: 10, label: "ทพ.พงษ์ พงศ์พฤกษา" },
                                { value: 11, label: "ทญ.อรวรรณ  ตรีวิมล" },
                                { value: 12, label: "ทญ.กนกพร  ไอรมณีรัตน์" },
                                { value: 13, label: "อ.ทพ.ดร.ชิษณุ  แจ้งศิริพันธ์" },
                                { value: 14, label: "รศ.ทญ.สุขนิภา วงศ์ทองศรี" },
                                { value: 15, label: "ทญ.จิราภรณ์  แต้วีระพิชัย" },
                                { value: 16, label: "ทญ.ผาณิต  บัณฑิตสิงห์" },
                                { value: 17, label: "ทญ.อาทิตยา  กายพันธุ์เลิศ" },
                                { value: 18, label: "ทญ.หทัยรัก  วันหนุน" },
                                { value: 19, label: "ทญ.นิลนรา อารีธรรมศิริกุล" },
                                { value: 20, label: "ทญ.สรัญญา  ธาวนพงษ์" },
                                { value: 21, label: "ทญ.กุมาริกา  พวงผกา" },
                                { value: 22, label: "ทญ.วัชรี เล้าชัยวัฒน์" },
                                { value: 23, label: "ทพ.ปุณยวีร์ วีระโสภณ" },
                                { value: 24, label: "ทพ.ฉัตรพล  แจ่มศิริโรจน์รัตน์" },
                                { value: 25, label: "ทพ.บวร คลองน้อย" },
                                { value: 26, label: "ทญ.พรทิพา  ศิริวนิชสุนทร" },
                                { value: 27, label: "ผศ.นพ.ทพ.ดร ชาญชาย วงศ์ชื่นสุนทร" },
                                { value: 28, label: "ทพ.วัชรศักดิ์ ตุมราศวิน" },
                                { value: 29, label: "ทพ.ดำรง บรรจงศิลป์" },
                                { value: 30, label: "ทญ.วาสนา" },
                                { value: 31, label: "ทญ.สิริรัตน์  อนันต์วิริยะพร" },
                            ]} />
                          </Grid>
                        </Grid>
                        <Grid container spacing={8} alignItems="flex-end">
                          <Grid item>
                          <img src={Tooth} height='24px' />
                          </Grid>
                          <Grid item>
                          <InputMunti label={`deparments`} handleChange={this.handleChange('deparments')} value={this.state.deparments} disableUnderline={false} placeholder="Symptom" suggestions={[
                                { label: "ทันตกรรมสำหรับเด็ก", value: 1 },
                                { label: "ทันตกรรมนอนกรน", value: 2 },
                                { label: "ทันตกรรมจัดฟัน", value: 3 },
                                { label: "ศัลยกรรมช่องปากและกระดูกขากรรไกร", value: 4 },
                                { label: "ทันตกรรมประดิษฐ์", value: 5 },
                                { label: "ทันตกรรมบดเคี้ยว", value: 6 },
                                { label: "ทันตกรรมหัตถการ", value: 7 },
                                { label: "ทันตกรรมรักษารากฟัน", value: 8 },
                                { label: "ทันตกรรมปริทันต์", value: 9 },
                                { label: "ทันตกรรมทั่วไป", value: 10 },
                            ]} />
                          </Grid>
                        </Grid>
                            
                        <Grid container spacing={8} alignItems="flex-end">
                          <Grid item>
                          <CalendarToday />
                          </Grid>
                          <Grid item>
                            <CalendaDatePicker />
                          </Grid>
                          </Grid>
                          <Grid container spacing={8} alignItems="flex-end">
                          <Grid item>
                          <Star />
                          </Grid>
                          <Grid item>
                          <TextField
                                id="standard-HN"
                                fullWidth
                                label="HN"
                                className={classes.textField}
                                value={this.state.name}
                                onChange={this.handleChange('HN')}
                                margin="normal"
                            />
                          </Grid>
                          </Grid>
                          
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

Search.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Search);
