import React from "react"
import PropTypes from "prop-types"
import { withStyles } from "@material-ui/core/styles"
import AddBoxIcon from '@material-ui/icons/AddBox'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import SearchIcon from '@material-ui/icons/Search'
import MoreIcon from '@material-ui/icons/More'
import MenuIcon from '@material-ui/icons/Menu'
import SettingsIcon from '@material-ui/icons/Settings'
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew'
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd'
import AccountCircleIcon from "@material-ui/icons/AccountCircle"
import { Link } from 'react-router-dom'
import Popover from '@material-ui/core/Popover'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Avatar from '@material-ui/core/Avatar'
import ListItemText from '@material-ui/core/ListItemText'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
const styles = theme => ({
    root: {
        width: "100%"
    },
    grow: {
        flexGrow: 1
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20
    },
    title: {
        display: "none",
        [theme.breakpoints.up("sm")]: {
            display: "block"
        }
    },
    search: {
        position: "relative",
        borderRadius: theme.shape.borderRadius,
        backgroundColor: "#c3e1e5",
        // "&:hover": {
        //     backgroundColor: fade(theme.palette.common.white, 0.25)
        // },
        // marginRight: theme.spacing.unit * 2,
        // marginLeft: 0,
        width: "80%",
        [theme.breakpoints.up("sm")]: {
            marginLeft: theme.spacing.unit,
            width: "auto"
        }
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: "100%",
        position: "absolute",
        pointerEvents: "none",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    inputRoot: {
        color: "inherit",
        width: "100%"
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        // paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create("width"),
        width: "100%",
        [theme.breakpoints.up("md")]: {
            // width: 200
        }
    },
    inputTimePicker: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        // paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create("width"),
        width: "100%",
        [theme.breakpoints.up("md")]: {
            // width: 200
        }
    },
    sectionDesktop: {
        display: "none",
        [theme.breakpoints.up("md")]: {
            display: "flex"
        }
    },
    sectionMobile: {
        display: "flex",
        [theme.breakpoints.up("md")]: {
            display: "none"
        }
    },
    key: {
        margin: 0,
        padding: 0
    },
    rootList: {
        width: "100%",
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper
    },
    grow: {
        flexGrow: 1,
    },
    textField: {
        // marginLeft: theme.spacing.unit,
        // marginRight: theme.spacing.unit,
        ":before": "",
        width: "100%",
    },
})

class PrimarySearchAppBar extends React.Component {
    state = {
        anchorEl: null,
        mobileMoreAnchorEl: null,
    }
    handleChange = name => value => {
        this.setState({ [name]: value })
    }
    handleDentist = e => {
        this.setState({ dentist: e })
    }
    handleClose = () => {
        this.setState({
            anchorEl: null
        });
    };

    handleProfileMenuOpen = event => {
        this.setState({ anchorEl: event.currentTarget })
    }

    handleMenuClose = () => {
        this.setState({ anchorEl: null })
        this.handleMobileMenuClose()
    }

    handleMobileMenuOpen = event => {
        this.setState({ mobileMoreAnchorEl: event.currentTarget })
    }

    handleMobileMenuClose = () => {
        this.setState({ mobileMoreAnchorEl: null })
    }

    render() {
        const { anchorEl, } = this.state
        const { classes, handleModel } = this.props
        const isMenuOpen = Boolean(anchorEl)
        const open = Boolean(anchorEl);
        // const isMobileMenuOpen = Boolean(mobileMoreAnchorEl)
        // const renderMenu = (
        //     <Menu
        //         anchorEl={anchorEl}
        //         anchorOrigin={{ vertical: "top", horizontal: "right" }}
        //         transformOrigin={{ vertical: "top", horizontal: "right" }}
        //         open={isMenuOpen}
        //         onClose={this.handleMenuClose}
        //     >
        //         <MenuItem onClick={this.handleClose}>Profile</MenuItem>
        //         <MenuItem onClick={this.handleClose}>My account</MenuItem>
        //     </Menu>
        // )

        // const renderMobileMenu = (
        //     <Menu
        //         anchorEl={mobileMoreAnchorEl}
        //         anchorOrigin={{ vertical: "top", horizontal: "right" }}
        //         transformOrigin={{ vertical: "top", horizontal: "right" }}
        //         open={isMobileMenuOpen}
        //         onClose={this.handleMobileMenuClose}
        //     >
        //         <MenuItem>
        //             <IconButton color="inherit">
        //                 <Badge
        //                     className={classes.margin}
        //                     badgeContent={4}
        //                     color="secondary"
        //                 >
        //                     <MailIcon />
        //                 </Badge>
        //             </IconButton>
        //             <p>Messages</p>
        //         </MenuItem>
        //         <MenuItem>
        //             <IconButton color="inherit">
        //                 <Badge
        //                     className={classes.margin}
        //                     badgeContent={11}
        //                     color="secondary"
        //                 >
        //                     <NotificationsIcon />
        //                 </Badge>
        //             </IconButton>
        //             <p>Notifications</p>
        //         </MenuItem>
        //         <MenuItem onClick={this.handleProfileMenuOpen}>
        //             <IconButton color="inherit">
        //                 <AccountCircleIcon />
        //             </IconButton>
        //             <p>Profile</p>
        //         </MenuItem>
        //     </Menu>
        // )

        return (
            <div className={classes.root}>
                <AppBar
                    style={{ backgroundColor: "#EFEFEF", color: "#000" }}
                    position="static"
                >
                    <Toolbar>
                        <img src="http://blueseas.co.th/img/logo.png" style={{ marginRight: "1%" }} />
                        <Typography variant="h6" style={{ width: "5%", display: "flex", color: "#3fa9f5" }} className={classes.grow}>BlueSeas
                            <Typography variant="body1" style={{ color: "#2e646d", fontSize: "10px", display: "contents" }}>
                                Enterprise
                            </Typography>
                        </Typography>


                        <div className={classes.grow} />
                        <div style={{ width: '5%' }}>
                            <Button
                                variant="text"
                                style={{ background: "#3fa9f5", width: "100%", padding: 0, color: "#fff" }}
                                onClick={handleModel}
                            >
                                <AddBoxIcon />
                                นัดหมาย
                            </Button>
                        </div>
                        <div className={classes.sectionDesktop}>
                            <IconButton
                                aria-owns={isMenuOpen ? "material-appbar" : null}
                                aria-haspopup="true"
                                onClick={this.handleProfileMenuOpen}
                                color="inherit"
                            >
                                <MenuIcon />
                            </IconButton>
                        </div>
                        <div className={classes.sectionMobile}>
                            <IconButton
                                aria-haspopup="true"
                                onClick={this.handleMobileMenuOpen}
                                color="inherit"
                            >
                                <MoreIcon />
                            </IconButton>
                        </div>
                        <Popover
                            id="simple-popper"
                            open={open}
                            anchorEl={anchorEl}
                            onClose={this.handleClose}
                            anchorOrigin={{
                                vertical: "bottom",
                                horizontal: "center"
                            }}
                            transformOrigin={{
                                vertical: "top",
                                horizontal: "center"
                            }}
                        >
                            <div className={classes.rootList}>
                                <List>
                                    <ListItem component={props => <Link to="/ForDentist" {...props} />} button>
                                        <Avatar>
                                            <AccountCircleIcon />
                                        </Avatar>
                                        <ListItemText primary="นพ.กันย์" secondary="พงษ์สามา" />
                                    </ListItem>
                                    <ListItem button>
                                        <Avatar>
                                            <AssignmentIndIcon />
                                        </Avatar>
                                        <ListItemText primary="Profile" />
                                    </ListItem>
                                    <ListItem button>
                                        <Avatar>
                                            <SettingsIcon />
                                        </Avatar>
                                        <ListItemText primary="Setting" />
                                    </ListItem>
                                    <ListItem button>
                                        <Avatar>
                                            <PowerSettingsNewIcon />
                                        </Avatar>
                                        <ListItemText primary="Logout" />
                                    </ListItem>
                                </List>
                            </div>
                        </Popover>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}

PrimarySearchAppBar.propTypes = {
    classes: PropTypes.object.isRequired
}

export default withStyles(styles)(PrimarySearchAppBar)
