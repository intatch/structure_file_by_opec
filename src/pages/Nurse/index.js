import React from 'react'
import { Switch, Route } from 'react-router-dom'
// import index from './index'
import Schedule from './Schedule'
const Nurse = () => (
    <Switch>
        <Route path='/nurse/Schedule' component={Schedule} />
    </Switch>
)

export default Nurse
