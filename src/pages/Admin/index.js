import React from 'react'
import { Switch, Route } from 'react-router-dom'
// import index from './index'
import Dashboard from './Dashboard'
const Admin = () => (
  <Switch>
    <Route exact path='/admin/Dashboard' component={Dashboard} />
  </Switch>
)

export default Admin
